package com.valentynalysenok.homework.homework9;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void shouldReturnCorrectToStringValueForHumanClass() {
        //given
        String[][] schedule = new String[][]{
                {DayOfWeek.MONDAY.day(), "Go to work"},
                {DayOfWeek.TUESDAY.day(), "Go to work"},
                {DayOfWeek.WEDNESDAY.day(), "Go to work"},
                {DayOfWeek.THURSDAY.day(), "Go to work"},
                {DayOfWeek.FRIDAY.day(), "Go to work"},
                {DayOfWeek.SATURDAY.day(), "Go to the gym"},
                {DayOfWeek.SUNDAY.day(), "Cleaning apartment"}
        };
        Human human = new Human("Ella", "Tuck", 1980, 123, schedule);
        //when
        String expectedResult = "Human{name: Ella, surname: Tuck, year: 1980, iq: 123,\n" +
                "My schedule on weekends: [[Monday, Go to work], [Tuesday, Go to work], " +
                "[Wednesday, Go to work], [Thursday, Go to work], [Friday, Go to work], " +
                "[Saturday, Go to the gym], [Sunday, Cleaning apartment]]}";
        String result = new String();
        result += "Human{";
        result += "name: " + human.getName();
        result += ", surname: " + human.getSurname();
        result += ", year: " + human.getYear();
        result += ", iq: " + human.getIq();
        result += ",\nMy schedule on weekends: " + Arrays.deepToString(human.getArrSchedule()) + "}";
        //then
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testEqualsAndHashCodeForHumanClass() {
        //given
        Human human1 = new Human("Margaret", "Thatcher", 1925, 123);
        Human human2 = new Human("Margaret", "Thatcher", 1925, 123);
        Human human3 = new Human("Marilyn", "Monroe", 1926, 123);
        //when
        //then
        assertEquals(human1, human2);
        assertNotEquals(human1, human3);
        assertNotEquals(human2, human3);
        assertTrue(human1.hashCode() == human2.hashCode());
        assertFalse(human1.hashCode() == human3.hashCode());
        assertFalse(human2.hashCode() == human3.hashCode());

    }


}
