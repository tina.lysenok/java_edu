package com.valentynalysenok.homework.homework13;

import com.valentynalysenok.homework.homework13.Human.Human;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CollectionFamilyDaoTest {

    @Test
    public void getAllFamiliesShouldReturnFamiliesList() {
        //given
        Human mother1 = new Human();
        Human father1 = new Human();
        Human mother2 = new Human();
        Human father2 = new Human();
        Family family1 = new Family(mother1, father1);
        Family family2 = new Family(mother2, father2);
        List<Family> families = new ArrayList<>();
        List<Family> families2 = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family1);
        familyService.getFamilyDao().saveFamily(family2);
        //when
        List<Family> expectedResult = families;
        List<Family> resultTrue = familyService.getFamilyDao().getAllFamilies();
        List<Family> resultFalse = families2;
        //then
        assertEquals(expectedResult, resultTrue);
        assertNotEquals(expectedResult, resultFalse);
    }

    @Test
    public void getFamilyByIndexShouldReturnFamilyWithIndexParameter() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        Family expectedResult1 = family;
        Family expectedResult2 = null;
        Family resultTrue = familyDao.getFamilyByIndex(0);
        Family resultFalse1 = familyDao.getFamilyByIndex(1);
        Family resultFalse2 = familyDao.getFamilyByIndex(-1);
        //then
        assertEquals(expectedResult1, resultTrue);
        assertEquals(expectedResult2, resultFalse1);
        assertEquals(expectedResult2, resultFalse2);
        assertNotEquals(expectedResult2, resultTrue);
    }

    @Test
    public void deleteFamilyByIndexShouldDeleteFamilyFromFamiliesListAndReturnBooleanValue() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        int expectedResult = families.size() - 1;
        boolean resultTrue = familyService.getFamilyDao().deleteFamilyByIndex(0);
        int result = families.size();
        boolean resultFalse = familyService.getFamilyDao().deleteFamilyByIndex(-1);
        boolean resultFalse2 = familyService.getFamilyDao().deleteFamilyByIndex(1);
        //then
        assertThat(resultTrue, is(true));
        assertThat(resultFalse, is(false));
        assertThat(resultFalse2, is(false));
        assertEquals(expectedResult, result);
    }

    @Test
    public void deleteFamilyByFamilyNameShouldDeleteFamilyFromFamiliesListAndReturnBooleanValue() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        Family family2 = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        familyService.getFamilyDao().saveFamily(family);
        //when
        int expectedResult = families.size() - 1;
        boolean resultTrue = familyService.getFamilyDao().deleteFamily(family);
        int result = families.size();
        boolean resultFalse = familyService.getFamilyDao().deleteFamily(family2);
        //then
        assertThat(resultTrue, is(true));
        assertThat(resultFalse, is(false));
        assertEquals(expectedResult, result);
    }

    @Test
    public void saveFamilyToFamiliesListAndReturnFamilyObject() {
        //given
        Human mother = new Human();
        Human father = new Human();
        Family family = new Family(mother, father);
        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        //when
        Family familyExpectedResult = null;
        Family familyResult = familyService.getFamilyDao().saveFamily(family);
        for (Family familyItem : families) {
            familyExpectedResult = familyItem;
        }
        int expectedResult = 1;
        int result = families.size();
        //then
        assertEquals(familyExpectedResult, familyResult);
        assertEquals(expectedResult, result);
    }
}
