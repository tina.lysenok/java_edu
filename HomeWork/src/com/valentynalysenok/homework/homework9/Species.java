package com.valentynalysenok.homework.homework9;

public enum Species {
    CAT("cat", false, 4, true),
    DOG("dog", false, 4, true),
    RABBIT("rabbit", false, 4, true),
    HAMSTER("hamster", false, 4, true),
    RAT("rat", false, 4, true),
    HEDGEHOG("hedgehog", false, 4, false),
    RACCOON("raccon", false, 4, true),
    PARROT("parrot", true, 2, false);

    private String speciesName;
    private boolean canFly;
    private int numLegs;
    private boolean hasFur;

    Species(String speciesName, boolean canFly, int numLegs, boolean hasFur) {
        this.speciesName = speciesName;
        this.canFly = canFly;
        this.numLegs = numLegs;
        this.hasFur = hasFur;
    }

    public String species() {
        return speciesName;
    }

    public boolean canFly() { return canFly; }

    public int numberOfLegs() {
        return numLegs;
    }

    public boolean hasFur() { return hasFur; }
}
