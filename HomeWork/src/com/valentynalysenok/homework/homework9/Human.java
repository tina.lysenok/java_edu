package com.valentynalysenok.homework.homework9;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    protected Family family;

    private String[][] arrSchedule = new String[][]{};

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public Human(String name, String surname, int year, int iq, String[][] arrSchedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
    }

    public Human(String name, String surname, int year, int iq, String[][] arrSchedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
        this.family = family;
    }

    ////пустой конструктор
    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getArrSchedule() {
        return arrSchedule;
    }

    public void setArrSchedule(String[][] arrSchedule) {
        this.arrSchedule = arrSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String describePet() {
        String petTricks;
        if (this.getFamily().getPet().getTrickLevel() <= 50 && this.getFamily().getPet().getTrickLevel() > 0) {
            petTricks = "almost not tricky";
        } else if (this.getFamily().getPet().getTrickLevel() > 50) {
            petTricks = "very tricky";
        } else {
            petTricks = "very clever and very tricky";
        }
        if (this.getFamily().getPet().getAge() == 0) {
            return "I have " + this.getFamily().getPet().getSpecies() + ", it's " + petTricks + ".";
        } else {
            return "I have " + this.getFamily().getPet().getSpecies() + ", it's " + this.getFamily().getPet().getAge()
                    + " years, it " + petTricks + ".";
        }
    }

    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        if (isTimeToFeedPet == true) {
            System.out.println("Hmm...I'll feed the " + this.getFamily().getPet().getNickname());
        } else {
            if (this.getFamily().getPet().getTrickLevel() > randomLevel) {
                System.out.println("Hmm...I'll feed the " + this.getFamily().getPet().getNickname());
            } else {
                System.out.println("I think, " + this.getFamily().getPet().getNickname() + " don't hungry.");
            }
        }
        if (this.getFamily().getPet().getTrickLevel() > randomLevel || isTimeToFeedPet == true) {
            return true;
        } else {
            return false;
        }
    }

    //Human{name='Name', surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "\nHuman{";
        aboutMyself += "name: " + this.name;
        aboutMyself += ", surname: " + this.surname;
        aboutMyself += ", year: " + this.year;
        aboutMyself += ", iq: " + this.iq;
        if (this.arrSchedule.length == 0) {
            aboutMyself += "}";
        } else {
            aboutMyself += ",\nMy schedule on weekends: " + Arrays.deepToString(this.arrSchedule) + "}";
        }
        return aboutMyself;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return  iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(year, human.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }

}
