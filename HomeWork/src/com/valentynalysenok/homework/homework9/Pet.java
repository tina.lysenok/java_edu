package com.valentynalysenok.homework.homework9;

import java.util.Arrays;
import java.util.Objects;

class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits = new String[]{};

    //конструктор, описывающий вид животного и его кличку
    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    //конструктор, описывающий все поля животного
    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = testTrickLevel(trickLevel);
        this.habits = habits;
    }

    //пустой конструктор
    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    //целое число от 0 до 100
    private int testTrickLevel(int level) {
        if (level < 0) {
            this.trickLevel = 0;
        } else if (level > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = level;
        }
        return this.trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {

        String aboutPet = "";
        aboutPet += this.species.species() + " {";
        aboutPet += "nickname: " + this.nickname;
        if (this.age == 0) {
        } else {
            aboutPet += ", age: " + this.age;
        }
        aboutPet += ", it can fly: " + this.species.canFly();
        aboutPet += ", it has fur: " + this.species.hasFur();
        aboutPet += ", it has " + this.species.numberOfLegs() + " legs";

        if (this.trickLevel == 0) {
        } else {
            aboutPet += ", trickLevel: " + this.trickLevel;
        }
        if (this.habits.length == 0) {
            aboutPet += " } ";
        } else {
            aboutPet += ", habits: " + Arrays.toString(this.habits) + "}";
        }

        return aboutPet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return  Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }

}

