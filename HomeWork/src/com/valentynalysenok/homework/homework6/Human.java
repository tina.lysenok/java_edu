package com.valentynalysenok.homework.homework6;

import java.util.Arrays;
import java.util.Random;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Pet pet;

    private String[][] arrSchedule = new String[][]{};

    public Human(String name, String surname, int year, int iq, String[][] arrSchedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
    }

    public Human(String name, String surname, int year, int iq, String[][] arrSchedule, Family family, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
        this.family = family;
        this.pet = pet;
    }

    ////пустой конструктор
    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getArrSchedule() {
        return arrSchedule;
    }

    public void setArrSchedule(String[][] arrSchedule) {
        this.arrSchedule = arrSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.println("Hi, " + this.pet.getNickname());
    }

    public String describePet() {
        String petTricks;
        if (this.pet.getTrickLevel() <= 50 && this.pet.getTrickLevel() > 0) {
            petTricks = "almost not tricky";
        } else if (this.pet.getTrickLevel() > 50) {
            petTricks = "very tricky";
        } else {
            petTricks = "very clever and very tricky";
        }

        if (this.pet.getAge() == 0) {
            return "I have " + this.pet.getSpecies() + ", it's " + petTricks + ".";
        } else {
            return "I have " + this.pet.getSpecies() + ", it's " + this.pet.getAge() + " years, it " + petTricks + ".";
        }
    }

    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        if (isTimeToFeedPet == true) {
            System.out.println("Hmm...I'll feed the " + this.pet.getNickname());
        } else {
            if (this.pet.getTrickLevel() > randomLevel) {
                System.out.println("Hmm...I'll feed the " + this.pet.getNickname());
            } else {
                System.out.println("I think, " + this.pet.getNickname() + " don't hungry.");
            }
        }
        if (this.pet.getTrickLevel() > randomLevel || isTimeToFeedPet == true) {
            return true;
        } else {
            return false;
        }
    }

    //Human{name='Name', surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "\nHuman{";
        aboutMyself += "name: " + this.name;
        aboutMyself += ", surname: " + this.surname;
        aboutMyself += ", year: " + this.year;
        aboutMyself += ", iq: " + this.iq;
        aboutMyself += ",\nMy schedule on weekends: " + Arrays.deepToString(this.arrSchedule) + "}";

        return aboutMyself;
    }

}
