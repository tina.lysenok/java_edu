package com.valentynalysenok.homework.homework7;

import java.util.Arrays;
import java.util.Objects;

class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits = new String[]{};

    //конструктор, описывающий вид животного и его кличку
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    //конструктор, описывающий все поля животного
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = testTrickLevel(trickLevel);
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    //целое число от 0 до 100
    private int testTrickLevel(int level) {
        if (level < 0) {
            this.trickLevel = 0;
        } else if (level > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = level;
        }
        return this.trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    //нестатический блоки инициализации
    {
        System.out.println("New Object Pet created");
    }

    //статический блоки инициализации
    static {
        System.out.println(Pet.class + " uploaded");
    }

    @Override
    public String toString() {

        String aboutPet = "";
        aboutPet += this.species + " {";
        aboutPet += "nickname: " + this.nickname;
        if (this.age == 0) {
        } else {
            aboutPet += ", age: " + this.age;
        }
        if (this.trickLevel == 0) {
        } else {
            aboutPet += ", trickLevel: " + this.trickLevel;
        }
        if (this.habits.length == 0) {
            aboutPet += " } ";
        } else {
            aboutPet += ", habits: " + Arrays.toString(this.habits) + "}";
        }

        return aboutPet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return  age == pet.age &&
                trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
}

