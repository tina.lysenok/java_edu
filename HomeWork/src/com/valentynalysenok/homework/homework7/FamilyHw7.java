package com.valentynalysenok.homework.homework7;

public class FamilyHw7 {
    public static void main(String[] args) {

        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        String[][] motherShedule = new String[][]{
                {"Saturday", "Go to the gym"},
                {"Sunday", "Cleaning apartment"}
        };
        String[][] fatherSchedule = new String[][]{
                {"Saturday", "Reading book"},
                {"Sunday", "Go to the gym"}
        };
        String[][] childrenSchedule = new String[][]{
                {"Saturday", "Take a rest"},
                {"Sunday", "Preparing for school"}
        };

        Human mother = new Human("Alisa", "Tuck", 1980, 123, motherShedule);
        Human father = new Human("Nikita", "Tuck", 1980, 123, fatherSchedule);

        Pet pet = new Pet("dog", "Jack", 11, 89, petHabits);

        Family family = new Family(mother, father, pet);
        Human bob = new Human("Bob", "Tuck", 2005, 111, childrenSchedule);
        Human rose = new Human("Rose", "Tuck", 2007, 100, childrenSchedule);
        family.addChild(bob);
        family.addChild(rose);

        //family.deleteChild(0);
        family.deleteChild(rose);
        System.out.println(family.countFamily());
        System.out.println(family.toString());
    }
}
