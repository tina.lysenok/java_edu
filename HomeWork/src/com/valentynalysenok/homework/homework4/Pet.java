package com.valentynalysenok.homework.homework4;

import java.util.Arrays;

public class Pet {

    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits = new String[]{};

    //целое число от 0 до 100
    public int testTrickLevel(int level) {
        if (level < 0) {
            trickLevel = 0;
        } else if (level > 100) {
            trickLevel = 100;
        } else {
            trickLevel = level;
        }
        return trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {

        String aboutPet = "";
        aboutPet += species + " { ";
        aboutPet += "nickname: " + nickname;
        aboutPet += ", age: " + age;
        aboutPet += ", trickLevel: " + trickLevel;
        aboutPet += ", habits: " + Arrays.toString(habits) + " } ";

        return aboutPet;
    }

}
