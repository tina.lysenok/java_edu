package com.valentynalysenok.homework.homework13.Animals;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {

    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public Pet(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
    }

    //конструктор, описывающий все поля животного
    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = testTrickLevel(trickLevel);
        this.habits = habits;
    }

    //пустой конструктор
    public Pet() {
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    //целое число от 0 до 100
    private int testTrickLevel(int level) {
        if (level < 0) {
            this.trickLevel = 0;
        } else if (level > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = level;
        }
        return this.trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    abstract void respond();

    @Override
        public String toString() {
            String aboutPet = "";
            aboutPet += "\n" + species.getSpeciesName() + " {";
            aboutPet += "nickname: " + nickname;
            if (this.age == 0) {
            } else {
                aboutPet += ", age: " + age;
            }
            aboutPet += ", it can fly: " + species.canFly();
            aboutPet += ", it has fur: " + species.hasFur();
            aboutPet += ", it has " + species.numberOfLegs() + " legs";

            if (trickLevel == 0) {
            } else {
                aboutPet += ", trickLevel: " + trickLevel;
            }
            if (habits.size() == 0) {
                aboutPet += " } ";
            } else {
                aboutPet += ", habits: " + this.habits + "}";
            }

            return aboutPet;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return  Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }


}