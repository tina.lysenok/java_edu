package com.valentynalysenok.homework.homework17;

import java.io.IOException;
import java.text.ParseException;

public class ConsoleAppStart {
    public static void main(String[] args) throws IOException, ParseException {

        ConsoleApp consoleApp = new ConsoleApp();
        consoleApp.start();

    }
}
