package com.valentynalysenok.homework.homework17;

import com.valentynalysenok.homework.homework16.Animals.Dog;
import com.valentynalysenok.homework.homework16.Animals.DomesticCat;
import com.valentynalysenok.homework.homework16.CollectionFamilyDao;
import com.valentynalysenok.homework.homework16.Family;
import com.valentynalysenok.homework.homework16.FamilyController;
import com.valentynalysenok.homework.homework16.FamilyDao;
import com.valentynalysenok.homework.homework16.FamilyService;
import com.valentynalysenok.homework.homework16.Human.Human;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class ConsoleApp {

    private BufferedReader readerText = new BufferedReader(new InputStreamReader(System.in));
    private BufferedReader readerNumber = new BufferedReader(new InputStreamReader(System.in));
    private String inputText;
    private int inputNumber;

    private void listOfMenuPointers() {
        System.out.println("Please choose menu number:");
        System.out.println("1 - create test Family data");
        System.out.println("2 - display all data from FamilyDB");
        System.out.println("3 - displaying Families from FamilyDB with quantity of members greater than user's input");
        System.out.println("4 - displaying Families from FamilyDB with quantity of members less than user's input");
        System.out.println("5 - displaying Families from FamilyDB with quantity of members that equals to user's input");
        System.out.println("6 - create and input new Family in FamilyDB");
        System.out.println("7 - remove Family from FamilyDB by Family ID");
        System.out.println("8 - born or adopt children to Families in FamilyDB");
        System.out.println("9 - delete children elder than user's input from Families in FamilyDB");
    }

    private List<Family> createTestFamiliesForFamilyDB(List<Family> families, FamilyController familyController) {

        Random random = new Random();
        int dateBirth = random.nextInt(10000000);
        int iq = random.nextInt(200);
        int petAge = random.nextInt(20);
        int trickyLevel = random.nextInt(100);

        for (int i = 0; i < 4; i++) {
            Human woman = new Human("TestWomanName" + i, "TestSurname" + i, dateBirth, iq);
            Human man = new Human("TestManName" + i, "TestSurname" + i, dateBirth, iq);
            DomesticCat cat = new DomesticCat("TestCatName", petAge, trickyLevel);
            Dog dog = new Dog("TestDogName", petAge, trickyLevel);
            familyController.getFamilyService().createNewFamily(woman, man);
            familyController.getFamilyService().addPet(i, cat);
            familyController.getFamilyService().addPet(i, dog);
        }
        return families;
    }

    private void showAllFamiliesFromFamilyDB(FamilyController familyController) {
        familyController.getFamilyService().displayAllFamilies();
    }


    private List<Family> getFamiliesWithMembersGreaterThan(FamilyController familyController) {
        try {
            inputNumber = Integer.parseInt(readerNumber.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        return familyController.getFamilyService().getFamiliesBiggerThan(inputNumber);
    }

    private List<Family> getFamiliesWithMembersLessThan(FamilyController familyController) {
        try {
            inputNumber = Integer.parseInt(readerNumber.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        return familyController.getFamilyService().getFamiliesLessThan(inputNumber);
    }

    private AtomicInteger countFamiliesWithMembersEqualsToInputNum(FamilyController familyController) {
        try {
            inputNumber = Integer.parseInt(readerNumber.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        return familyController.getFamilyService().countFamiliesWithMemberNumber(inputNumber);
    }

    private Family createNewFamilyAndAddToFamilyDB(FamilyController familyController) throws IOException, ParseException {

        int inputMotherDayOfBirth;
        int inputFatherDayOfBirth;
        int inputMotherMonthOfBirth;
        int inputFatherMonthOfBirth;
        int inputMotherYearOfBirth;
        int inputFatherYearOfBirth;
        int inputMotherIq;
        int inputFatherIq;
        BufferedReader readerText = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter mother's name:");
        String inputMotherName = readerText.readLine();
        System.out.println("Please enter mother's surname:");
        String inputMotherSurname = readerText.readLine();
        System.out.println("Please enter mother's day of birth:");
        try {
            inputMotherDayOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputMotherDayOfBirth = 0;
        }
        System.out.println("Please enter mother's month of birth:");
        try {
            inputMotherMonthOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputMotherMonthOfBirth = 0;
        }
        System.out.println("Please enter mother's year of birth:");
        try {
            inputMotherYearOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputMotherYearOfBirth = 0;
        }
        System.out.println("Please enter mother's iq:");
        try {
            inputMotherIq = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputMotherIq = 0;
        }
        System.out.println();
        System.out.println("Please enter father's name:");
        String inputFatherSurname = readerText.readLine();
        System.out.println("Please enter father's surname:");
        String inputFatherName = readerText.readLine();
        System.out.println("Please enter father's day of birth:");
        try {
            inputFatherDayOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputFatherDayOfBirth = 0;
        }
        System.out.println("Please enter father's month of birth:");
        try {
            inputFatherMonthOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputFatherMonthOfBirth = 0;
        }
        System.out.println("Please enter father's year of birth:");
        try {
            inputFatherYearOfBirth = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputFatherYearOfBirth = 0;
        }
        System.out.println("Please enter father's iq:");
        try {
            inputFatherIq = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            inputFatherIq = 0;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateOfMother = dateFormat.parse(inputMotherDayOfBirth
                + "/" + inputMotherMonthOfBirth
                + "/" + inputMotherYearOfBirth);
        Date dateOfFather = dateFormat.parse(inputFatherDayOfBirth
                + "/" + inputFatherMonthOfBirth
                + "/" + inputFatherYearOfBirth);
        long dateMothersBirth = dateOfMother.getTime();
        long dateFathersBirth = dateOfFather.getTime();

        Human mother = new Human(inputMotherName, inputMotherSurname, dateMothersBirth, inputMotherIq);
        Human father = new Human(inputFatherName, inputFatherSurname, dateFathersBirth, inputFatherIq);

        return familyController.getFamilyService().createNewFamily(mother, father);
    }

    private boolean deleteFamilyByIndex(FamilyController familyController) {
        try {
            inputNumber = Integer.parseInt(readerNumber.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        boolean resultOfFamilyDeletion = familyController
                .getFamilyService()
                .getFamilyDao()
                .deleteFamilyByIndex(inputNumber);
        String result = (!resultOfFamilyDeletion) ? "No such Family ID in DB." : "Family was deleted from DB.";
        System.out.println(result);
        return resultOfFamilyDeletion;
    }

    private Family getInfoAboutBornChildAndAddToFamilyDB(FamilyController familyController) throws IOException {
        int familyId = 0;
        Family family = null;
        try {
            System.out.println("Please enter family ID:");
            familyId = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        System.out.println("Please enter name for boy:");
        String boyName = readerText.readLine();
        System.out.println("Please enter name for girl:");
        String girlName = readerText.readLine();
        try {
            family = familyController.getFamilyService().bornChild(familyId, boyName, girlName);
        } catch (FamilyOverflowException e) {
            System.out.println("Couldn't add child to your family because family members capacity is " + e.getFamilyCapacity());
        }
        if (familyController.getFamilyService().getFamilyDao().getAllFamilies().size() == 0) {
            System.out.println("Child wasn't born. There are no families in FamilyDB.");
        } else {
            System.out.println("Child was born.");
        }
        return family;
    }

    private Family getInfoAboutAdoptChildAndAddToFamilyDB(FamilyController familyController) throws IOException {
        int familyId = 0;
        int childIq;
        Family family = null;
        try {
            System.out.println("Please enter family ID:");
            familyId = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        System.out.println("Please enter child surname:");
        String childSurname = readerText.readLine();
        System.out.println("Please enter child name:");
        String childName = readerText.readLine();
        System.out.println("Please enter child date of birth only by using format DD/MM/YYYY:");
        String childBirthYear = readerText.readLine();
        System.out.println("Please enter child iq:");
        try {
            childIq = Integer.parseInt(readerNumber.readLine());
        } catch (NumberFormatException e) {
            childIq = 0;
        }
        Human child = new Human(childName, childSurname, childBirthYear, childIq);
        try {
            family = familyController.getFamilyService().adoptChild(familyId, child);
        } catch (FamilyOverflowException e) {
            System.out.println("Couldn't add child to your family because family members capacity is " + e.getFamilyCapacity());
        }
        if (familyController.getFamilyService().getFamilyDao().getAllFamilies().size() == 0) {
            System.out.println("Child wasn't adopted. There are no families in FamilyDB.");
        } else {
            System.out.println("Child was adopted.");
        }
        return family;
    }

    private List<Family> deleteAllChildrenOlderThen(FamilyController familyController) {
        try {
            inputNumber = Integer.parseInt(readerNumber.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Error: String input.");
        }
        return familyController.getFamilyService().deleteAllChildrenOlderThen(inputNumber);
    }

    public void start() throws IOException, ParseException {

        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        while (true) {

            System.out.println("Please choose menu or exit:");

            inputText = readerText.readLine().toLowerCase().trim();
            switch (inputText) {
                case "menu":
                    listOfMenuPointers();
                    break;
                case "exit":
                    return;
                case "":
                    return;
            }

            try {
                inputNumber = Integer.parseInt(readerNumber.readLine());
            } catch (NumberFormatException e) {
                System.out.println();
            }

            switch (inputNumber) {
                case 1:
                    createTestFamiliesForFamilyDB(families, familyController);
                    System.out.println("Creation test families data ended.");
                    break;
                case 2:
                    System.out.println("Family DB:");
                    showAllFamiliesFromFamilyDB(familyController);
                    break;
                case 3:
                    System.out.println("Please enter number for displaying families " +
                            "with number of members greater than your input:");
                    getFamiliesWithMembersGreaterThan(familyController);
                    break;
                case 4:
                    System.out.println("Please enter number for displaying families " +
                            "with number of members less than your input:");
                    getFamiliesWithMembersLessThan(familyController);
                    break;
                case 5:
                    System.out.println("Please enter number for counting families " +
                            "with number of members which equals to your input:");
                    System.out.println(countFamiliesWithMembersEqualsToInputNum(familyController));
                    break;
                case 6:
                    System.out.println("You chosen new family creation.");
                    createNewFamilyAndAddToFamilyDB(familyController);
                    System.out.println("Family was added into Family DB.");
                    break;
                case 7:
                    System.out.println("Please enter number for deleting family from DB by ID:");
                    deleteFamilyByIndex(familyController);
                    break;
                case 8:
                    System.out.println("Please enter 1 for born child or 2 for adopt child:");
                    inputNumber = Integer.parseInt(readerNumber.readLine());
                    switch (inputNumber) {
                        case 1:
                            getInfoAboutBornChildAndAddToFamilyDB(familyController);
                            break;
                        case 2:
                            getInfoAboutAdoptChildAndAddToFamilyDB(familyController);
                            break;
                    }
                    break;
                case 9:
                    System.out.println("Please enter age of child for his/her removing from Family:");
                    deleteAllChildrenOlderThen(familyController);
                    break;
                default:
                    System.out.println("Unknown choice. Try again.");
                    break;
            }
        }
    }
}