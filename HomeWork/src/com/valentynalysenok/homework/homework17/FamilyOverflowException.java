package com.valentynalysenok.homework.homework17;

public class FamilyOverflowException extends RuntimeException {

    private int familyCapacity;

    public FamilyOverflowException(int familyCapacity) {
        this.familyCapacity = familyCapacity;
    }

    public int getFamilyCapacity() {
        return familyCapacity;
    }
}
