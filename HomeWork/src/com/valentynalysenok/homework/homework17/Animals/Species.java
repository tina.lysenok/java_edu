package com.valentynalysenok.homework.homework17.Animals;

public enum Species {
    DOMESTICCAT("Domestic Cat", false, 4, true),
    DOG("Dog", false, 4, true),
    ROBOCAT("Robo Cat", false, 4, false),
    FISH("Fish", false, 0, false),
    RABBIT("Rabbit", false, 4, true),
    HAMSTER("Hamster", false, 4, true),
    RAT("Rat", false, 4, true),
    HEDGEHOG("Hedgehog", false, 4, false),
    RACCOON("Raccon", false, 4, true),
    PARROT("Parrot", true, 2, false),
    UNKNOWN;

    private String speciesName;
    private boolean flyingPower;
    private int numLegs;
    private boolean furEffect;

    Species(String speciesName, boolean flyingPower, int numLegs, boolean furEffect) {
        this.speciesName = speciesName;
        this.flyingPower = flyingPower;
        this.numLegs = numLegs;
        this.furEffect = furEffect;
    }

    Species() {

    }

    public String getSpeciesName() {
        return speciesName;
    }

    public boolean canFly() { return flyingPower; }

    public int numberOfLegs() {
        return numLegs;
    }

    public boolean hasFur() { return furEffect; }
}
