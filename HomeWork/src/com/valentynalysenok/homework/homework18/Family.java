package com.valentynalysenok.homework.homework18;

import com.valentynalysenok.homework.homework18.Animals.Pet;
import com.valentynalysenok.homework.homework18.Human.Human;
import com.valentynalysenok.homework.homework18.Human.Man;
import com.valentynalysenok.homework.homework18.Human.Woman;

import java.util.*;

public class Family implements HumanCreator {

    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family() {}

    public Family(Human mother, Human father, Set<Pet> pets) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
        }
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<>();
        this.pets = pets;
    }

    public Family(Human mother, Human father) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
        }
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public void addChild(Human child) {
        this.children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) return false;
        else children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        if (!children.contains(child)) return false;
        else children.remove(child);
        return true;
    }

    public int countFamily() {
        int count = 0;
        count += this.children.size();
        if (this.mother != null && this.father != null) {
            count += 2;
        } else if (this.mother == null || this.father == null) {
            count += 1;
        } else {
            count += 0;
        }
        return count;
    }

    @Override
    public Human bornChild() {
        Human child = defineGender();
        child.setFamily(this);
        child.setName(chooseName(child));
        child.setSurname(father.getSurname());
        child.setIq(defineIq());
        //child.setBirthDate();
        addChild(child);
        return child;
    }

    private Human defineGender() {
        Random r = new Random();
        int probability = r.nextInt();
        if (probability <= 0.5) {
            return new Woman();
        } else {
            return new Man();
        }
    }

    private String chooseName(Human child) {
        String name = new String();
        Random random = new Random();
        int r = random.nextInt(namesBoys.length);
        if (child instanceof Man) {
            for (int i = 0; i < namesBoys.length; i++) {
                name = namesBoys[r];
            }
        } else {
            for (int i = 0; i < namesGirls.length; i++) {
                name = namesGirls[r];
            }
        }
        return name;
    }

    private int defineIq() {
        return (this.mother.getIq() + this.father.getIq()) / 2;
    }

    public void prettyFormat() {
        String aboutFamilyPrettyFormat = "family: ";
        aboutFamilyPrettyFormat += (mother != null) ?
                "\n\tmother: " + mother.prettyFormat() + "," : "\n\tmother: " + null + ",";
        aboutFamilyPrettyFormat += (father != null) ?
                "\n\tfather: " + father.prettyFormat() + "," : "\n\tfather: " + null + ",";
        aboutFamilyPrettyFormat += "\n\tchildren: ";
        for (Human child : children) {
            aboutFamilyPrettyFormat += (child instanceof Man) ?
                    "\n\t\tboy: " + child.prettyFormat() : "\n\t\tgirl: " + child.prettyFormat();
        }
        aboutFamilyPrettyFormat += "\n\tpets: [";
        if (pets != null) {
            for (Pet pet : pets) {
                aboutFamilyPrettyFormat += pet.prettyFormat();
            }
        } else {
            aboutFamilyPrettyFormat += null;
        }
        System.out.println(aboutFamilyPrettyFormat + "]");
    }

    @Override
    public String toString() {
        String aboutFamily = "";
        aboutFamily += "\nFamily {";

        if (this.mother != null) {
            aboutFamily += "\nMother: " + this.mother;
        } else {
            aboutFamily += "Mother: " + null;
        }
        if (this.father != null) {
            aboutFamily += "\nFather: " + this.father;
        } else {
            aboutFamily += "Father: " + null + ", ";
        }

        aboutFamily += "\nChildren: " + this.children;

        if (this.pets == null || this.pets.size() == 0) {
            aboutFamily += 0;
        } else {
            aboutFamily += ";\nPet: " + this.pets + "}";
        }
        return aboutFamily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }
}
