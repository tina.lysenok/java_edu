package com.valentynalysenok.homework.homework18;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamilyByIndex(int index);
    boolean deleteFamily(Family family);
    Family saveFamily(Family family);
    void loadData(List<Family> families);

}
