package com.valentynalysenok.homework.homework18;

import com.valentynalysenok.homework.homework18.Animals.Pet;
import com.valentynalysenok.homework.homework18.Human.Human;
import com.valentynalysenok.homework.homework18.Human.Man;
import com.valentynalysenok.homework.homework18.Human.Woman;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void displayAllFamilies() {
        if (familyDao.getAllFamilies() == null || familyDao.getAllFamilies().size() == 0) {
            System.out.println("The list of families is empty.");
        } else {
            familyDao.getAllFamilies().forEach(family -> family.prettyFormat());
        }
    }

    public List<Family> getFamiliesBiggerThan(int quantity) {
        List<Family> familiesBiggerThanQuantity = new ArrayList<>();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() > quantity) {
                familiesBiggerThanQuantity.add(family);
                family.prettyFormat();
            }
        });
        if (familiesBiggerThanQuantity.size() == 0) {
            System.out.println("No such families");
        }
        return familiesBiggerThanQuantity;
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        List<Family> familiesLessThanQuantity = new ArrayList<>();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() < quantity) {
                familiesLessThanQuantity.add(family);
                family.prettyFormat();
            }
        });
        if (familiesLessThanQuantity.size() == 0) {
            System.out.println("No such families");
        }
        return familiesLessThanQuantity;
    }

    public AtomicInteger countFamiliesWithMemberNumber(int quantity) {
        AtomicInteger counter = new AtomicInteger();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() == quantity) {
                counter.getAndIncrement();
            }
        });
        return counter;
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        int familyCapacity = 6;
        if (family.countFamily() > familyCapacity) {
            Human child = defineGender();
            family.addChild(child);
            child.setFamily(family);
            if (child instanceof Man) {
                child.setName(boyName);
            } else {
                child.setName(girlName);
            }
            child.setSurname(family.getFather().getSurname());
            child.setBirthDate(new Date().getTime());
            child.setIq(((family.getFather().getIq() + family.getMother().getIq()) / 2));
        } else {
            throw new FamilyOverflowException(familyCapacity);
        }
        return family;
    }

    public Family bornChild(int familyId, String boyName, String girlName) {
        int familyCapacity = 3;
        List<Family> families = familyDao.getAllFamilies();
        Family family;
        if (families != null && familyId >= 0 && familyId < families.size()) {
            family = familyDao.getFamilyByIndex(familyId);
            if (family.countFamily() < familyCapacity) {
                Human child = defineGender();
                family.addChild(child);
                child.setFamily(family);
                if (child instanceof Man) {
                    child.setName(boyName);
                } else {
                    child.setName(girlName);
                }
                child.setSurname(family.getFather().getSurname());
                child.setBirthDate(new Date().getTime());
                child.setIq(((family.getFather().getIq() + family.getMother().getIq()) / 2));
            } else {
                throw new FamilyOverflowException(familyCapacity);
            }
        } else {
            System.out.println("Your FamilyDB is empty or family index is wrong.");
            return null;
        }
        return family;
    }

    private Human defineGender() {
        Random r = new Random();
        int probability = r.nextInt();
        if (probability <= 0.5) return new Woman();
        else return new Man();
    }

    public Family adoptChild(Family family, Human child) {
        int familyCapacity = 6;
        if (family.countFamily() < familyCapacity) {
            child.setSurname(family.getFather().getSurname());
            family.addChild(child);
            child.setFamily(family);
        } else {
            throw new FamilyOverflowException(familyCapacity);
        }
        return family;
    }

    public Family adoptChild(int familyId, Human child) {
        int familyCapacity = 3;
        Family family;
        List<Family> families = familyDao.getAllFamilies();
        if (families != null && familyId >= 0 && familyId < families.size()) {
            family = familyDao.getFamilyByIndex(familyId);
            if (family.countFamily() < familyCapacity) {
                child.setSurname(family.getFather().getSurname());
                family.addChild(child);
                child.setFamily(family);
            } else {
                throw new FamilyOverflowException(familyCapacity);
            }
        } else {
            System.out.println("Your FamilyDB is empty or family index is wrong.");
            return null;
        }
        return family;
    }

    public List<Family> deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(family -> {
            for (Iterator<Human> iter = family.getChildren().iterator(); iter.hasNext(); ) {
                Human child = iter.next();
                try {
                    if (child.getHumanAge() >= age) {
                        iter.remove();
                        System.out.println("The child was deleted from next family/ies:");
                        family.prettyFormat();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        return familyDao.getAllFamilies();
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }


    public Set<Pet> getPets(int familyIndex) {
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            return null;
        } else {
            return familyDao.getAllFamilies().get(familyIndex).getPets();
        }
    }

    public Family addPet(int familyIndex, Pet pet) {
        Family family;
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            family = null;
        } else {
            family = familyDao.getAllFamilies().get(familyIndex);
            if (family.getPets() == null) {
                Set<Pet> pets = new HashSet<>();
                pets.add(pet);
                family.setPets(pets);
            } else {
                family.getPets().add(pet);
            }
        }
        return family;
    }

    public void loadData(List<Family> families) {
        try {
            FileWriter writer = new FileWriter("FamilyDB.txt");
            for(Family family : families) {
                writer.write(family.toString());
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
