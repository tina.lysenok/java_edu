package com.valentynalysenok.homework.homework18;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families;
    FamilyLogger logger = new FamilyLogger();

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        logger.info("show all families data from FamilyDB");
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            logger.error("couldn't get family from FamilyDB");
            return null;
        } else {
            logger.info("show family data by id information from FamilyDB");
            return families.get(index);
        }
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) {
            logger.error("family deletion is failed ");
            return false;
        } else {
            families.remove(index);
            logger.info("delete family data by id information from FamilyDB");
        }
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        boolean result = families.remove(family);
        if (!result) {
            logger.error("family deletion is failed");
        } else {
            logger.info("show family data by object information from FamilyDB");
        }
        return result;
    }

    @Override
    public Family saveFamily(Family family) {
        if (families.indexOf(family) == -1) {
            families.add(family);
            logger.info("save family data into FamilyDB");
        } else {
            families.set(families.indexOf(family), family);
            logger.info("re-save family data into FamilyDB");
        }
        return family;
    }

    @Override
    public void loadData(List<Family> families) {
        try {
            FileWriter writer = new FileWriter("FamilyDB.txt", true);
            for (Family family : families) {
                writer.write(family.toString());
                logger.info("load family data into FamilyDB");
            }
            writer.close();
        } catch (FileNotFoundException e) {
            logger.error("no file FamilyDB.txt");
        } catch (IOException e) {
            logger.error("unknown input/output");
        }
    }
}
