package com.valentynalysenok.homework.homework11.Human;

import com.valentynalysenok.homework.homework11.Animals.Pet;

import java.util.ArrayList;
import java.util.List;

public final class Man extends Human {

    public void greetPet() {
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            System.out.println("Hi, " + pet.getNickname() + " I'm your daddy, do you want to eat + " +
                    "smth delicious?");
        }
    }

    public void repairCar() {
        System.out.println("I'm enjoy of repairing my car");
    }

}
