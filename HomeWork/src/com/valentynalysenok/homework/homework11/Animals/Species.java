package com.valentynalysenok.homework.homework11.Animals;

public enum Species {
    DOMESTICCAT("Domestic Cat", false, 4, true),
    DOG("dog", false, 4, true),
    ROBOCAT("Robo Cat", false, 4, false),
    FISH("fish", false, 0, false),
    RABBIT("rabbit", false, 4, true),
    HAMSTER("hamster", false, 4, true),
    RAT("rat", false, 4, true),
    HEDGEHOG("hedgehog", false, 4, false),
    RACCOON("raccon", false, 4, true),
    PARROT("parrot", true, 2, false),
    UNKNOWN;

    private String speciesName;
    private boolean flyingPower;
    private int numLegs;
    private boolean furEffect;

    Species(String speciesName, boolean flyingPower, int numLegs, boolean furEffect) {
        this.speciesName = speciesName;
        this.flyingPower = flyingPower;
        this.numLegs = numLegs;
        this.furEffect = furEffect;
    }

    Species() {

    }

    public String getSpeciesName() {
        return speciesName;
    }

    public boolean canFly() { return flyingPower; }

    public int numberOfLegs() {
        return numLegs;
    }

    public boolean hasFur() { return furEffect; }
}
