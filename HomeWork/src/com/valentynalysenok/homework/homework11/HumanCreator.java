package com.valentynalysenok.homework.homework11;

import com.valentynalysenok.homework.homework11.Human.Human;

public interface HumanCreator {

    String[] namesBoys = new String[] {"Alex", "Bob", "Ben"};
    String[] namesGirls = new String[] {"Anna", "Tiffany", "Lisa"};

    Human bornChild();

}
