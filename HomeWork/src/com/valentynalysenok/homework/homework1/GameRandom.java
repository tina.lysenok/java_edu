package com.valentynalysenok.homework.homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.lang.String;

public class GameRandom {

    public static void main(String[] args) {

        int[] array = new int[]{};

        //генерирую рандомное число
        Random random = new Random();
        int numRandom = random.nextInt(100);
        System.out.println("Random is " + numRandom); //подсказка при проверке работы кода

        System.out.println("Let the game begin!");

        //спрашиваю и записываю имя юзера в переменную
        System.out.println("Please enter your name:");
        Scanner enterName = new Scanner(System.in);
        String nameUser = enterName.nextLine();

        //спрашиваю и записываю в переменную число, которое ввел юзер
        System.out.println("Please enter the number from 0 to 100:");
        Scanner enterNumber = new Scanner(System.in);
        int numUser = enterNumber.nextInt();

        //записываю данное число в пустой массив используя метод addNumberToArray
        array = addNumberToArray(array, numUser);

        //бессконечный цикл до момента, когда пользователь угадает число
        while (numRandom > numUser || numRandom < numUser || numRandom == numUser) {

            if (numRandom > numUser) {
                System.out.println("Your number is too small. Please, try again...");
                numUser = enterNumber.nextInt();
                array = addNumberToArray(array, numUser);
            } else if (numRandom < numUser) {
                System.out.println("Your number is too big. Please, try again...");
                numUser = enterNumber.nextInt();
                array = addNumberToArray(array, numUser);
            } else {
                System.out.println("Congratulations, " + nameUser);
                System.out.println("Your numbers: " + sortArrayFromMinToMax(array));

                break;
            }
        }

    }

    //сортировка массива чисел
    public static String sortArrayFromMinToMax(int[] arr) {
        //Bubble Sort
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return Arrays.toString(arr);
    }

    //добавляю числа, введенные пользователем в массив с ограничением диапазона [0, 100]
    public static int[] addNumberToArray(int[] arr, int number) {
        if (number > 100) {
            System.out.println("Your number should be less than 100");
        } else if (number < 0) {
            System.out.println("Your number should be greater than 0");
        } else {
            arr = Arrays.copyOf(arr, arr.length + 1);
            arr[arr.length - 1] = number;
        }
        return arr;
    }
}
