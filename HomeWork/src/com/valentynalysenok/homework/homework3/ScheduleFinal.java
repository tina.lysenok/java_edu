package com.valentynalysenok.homework.homework3;

import java.util.Scanner;

public class ScheduleFinal {
    public static void main(String[] args) {

        boolean flag = true;

        Scanner strEnter = new Scanner(System.in);

        String[][] schedule = new String[][]{
                // 0       1
                {"Sunday", "Play with my cat. Feed the cat"},            //0
                {"Monday", "Brush my cat. Feed the cat"},                //1
                {"Tuesday", "Speak with my cat. Feed the cat"},          //2
                {"Wednesday", "Wash my cat. Feed the cat"},              //3
                {"Thursday", "Feed the cat"},                            //4
                {"Friday", "Feed the cat"},                              //5
                {"Saturday", "To hunt birds with my cat. Feed the cat"}  //6
        };

        while (flag) {
            System.out.println("Please, input the day of the week:");
            String str = strEnter.nextLine().trim().toLowerCase();
            switch (str) {
                case "sunday":
                    System.out.println("Your tasks for " + schedule[0][0] + ": " + "'" + schedule[0][1] + "'");
                    break;
                case "monday":
                    System.out.println("Your tasks for " + schedule[1][0] + ": " + "'" + schedule[1][1] + "'");
                    break;
                case "tuesday":
                    System.out.println("Your tasks for " + schedule[2][0] + ": " + "'" + schedule[2][1] + "'");
                    break;
                case "wednesday":
                    System.out.println("Your tasks for " + schedule[3][0] + ": " + "'" + schedule[3][1] + "'");
                    break;
                case "thursday":
                    System.out.println("Your tasks for " + schedule[4][0] + ": " + "'" + schedule[4][1] + "'");
                    break;
                case "friday":
                    System.out.println("Your tasks for " + schedule[5][0] + ": " + "'" + schedule[5][1] + "'");
                    break;
                case "saturday":
                    System.out.println("Your tasks for " + schedule[6][0] + ": " + "'" + schedule[6][1] + "'");
                    break;
                case "exit":
                    System.out.println("Good bay!");
                    flag = false;
                    break;
                default:
                    if (!(str.contains("change") || str.contains("reschedule"))) {
                        System.out.println("Sorry, I don't understand you, please try again.");
                        break;
                    }
            }

            if (str.contains("change") || str.contains("reschedule")) {

                String newTask;

                if (str.contains("sunday")) {
                    System.out.println("Please, input new tasks for Sunday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[0][1] = newTask;
                } else if (str.contains("monday")) {
                    System.out.println("Please, input new tasks for Monday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[1][1] = newTask;
                } else if (str.contains("tuesday")) {
                    System.out.println("Please, input new tasks for Tuesday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[2][1] = newTask;
                } else if (str.contains("wednesday")) {
                    System.out.println("Please, input new tasks for Wednesday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[3][1] = newTask;
                } else if (str.contains("thursday")) {
                    System.out.println("Please, input new tasks for Thursday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[4][1] = newTask;
                } else if (str.contains("friday")) {
                    System.out.println("Please, input new tasks for Friday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[5][1] = newTask;
                } else if (str.contains("saturday")) {
                    System.out.println("Please, input new tasks for Saturday:");
                    newTask = strEnter.nextLine().trim();
                    schedule[6][1] = newTask;
                }
            }
        }
    }
}

