package com.valentynalysenok.homework.homework3;

import java.util.Scanner;

public class ScheduleWoSwitch {
    public static void main(String[] args) {
        Scanner dayWeekEnter = new Scanner(System.in);

        String[][] schedule = new String[][]{
                // 0       1
                {"Sunday", "testSun"},   //0
                {"Monday", "testMon"},   //1
                {"Tuesday", "testTue"},  //2
                {"Wednesday", "testWed"},//3
                {"Thursday", "testThu"}, //4
                {"Friday", "testFri"},   //5
                {"Saturday", "testSat"}  //6
        };

        System.out.println("Please, input the day of the week:");
        String dayWeek = dayWeekEnter.nextLine().trim();

       /* System.out.println("Please, input new tasks for " + dayWeek);
        String newTask = dayWeekEnter.nextLine().trim();*/

        boolean flag = true;

        while (flag) {

            for (int i = 0; i < schedule.length; i++) {
                for (int j = 0; j < 2; j++) {
                    if (dayWeek.equalsIgnoreCase("Sunday")) {
                        if (schedule[i][j].equals("Sunday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Monday")) {
                        if (schedule[i][j].equals("Monday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Tuesday")) {
                        if (schedule[i][j].equals("Tuesday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Wednesday")) {
                        if (schedule[i][j].equals("Wednesday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Thursday")) {
                        if (schedule[i][j].equals("Thursday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Friday")) {
                        if (schedule[i][j].equals("Friday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } else if (dayWeek.equalsIgnoreCase("Saturday")) {
                        if (schedule[i][j].equals("Saturday")) {
                            System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                            System.out.println("Please, input the day of the week:");
                            dayWeek = dayWeekEnter.nextLine().trim();
                        }
                    } /*else if (dayWeek.equalsIgnoreCase("change" + schedule[i][j])) {
                        schedule[i][j + 1] = newTask;
                        System.out.println("Your tasks for " + schedule[i][j] + ": " + schedule[i][j + 1]);
                        System.out.println("Please, input the day of the week:");
                        dayWeek = dayWeekEnter.nextLine().trim();
                    }*/else if (!dayWeek.equalsIgnoreCase(schedule[i][j])) {
                        System.out.println("Sorry, I don't understand you, please try again.");
                        System.out.println("Please, input the day of the week:");
                        dayWeek = dayWeekEnter.nextLine().trim();
                    }
                }
            }
            if (dayWeek.equalsIgnoreCase("exit")) {
                System.out.println("Good bay!");
                flag = false;
            }
        }
    }
}
