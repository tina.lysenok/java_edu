package com.valentynalysenok.homework.homework14.Animals;

public class Fish extends Pet {

    public Fish(String nickname, int age) {
        super(nickname, age);
        setSpecies(Species.FISH);
    }

    @Override
    void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }
}
