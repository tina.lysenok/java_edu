package com.valentynalysenok.homework.homework14;

public enum DayOfWeek {

    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private String dayOfWeekName;

    DayOfWeek(String dayOfWeekName) {
        this.dayOfWeekName = dayOfWeekName;
    }

    public String day() {
        return dayOfWeekName;
    }

}
