package com.valentynalysenok.homework.homework14;

import com.valentynalysenok.homework.homework14.Animals.Pet;
import com.valentynalysenok.homework.homework14.Human.Human;
import com.valentynalysenok.homework.homework14.Human.Man;
import com.valentynalysenok.homework.homework14.Human.Woman;

import java.text.ParseException;
import java.util.*;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        for (int i = 0; i < families.size(); i++) {
            int familyIndex = i + 1;
            System.out.println(familyIndex + " " + families.get(i));
        }
    }

    public List<Family> getFamiliesBiggerThan(int quantity) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> familiesBiggerThanQuantity = new ArrayList<>();
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).countFamily() > quantity) {
                Family family = families.get(i);
                familiesBiggerThanQuantity.add(family);
            } else if (families.get(i).countFamily() < quantity) {
                familiesBiggerThanQuantity = null;
            }
        }
        return familiesBiggerThanQuantity;
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        List<Family> families = familyDao.getAllFamilies();
        List<Family> familiesLessThanQuantity = new ArrayList<>();
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).countFamily() < quantity) {
                Family family = families.get(i);
                familiesLessThanQuantity.add(family);
            } else if (families.get(i).countFamily() > quantity) {
                familiesLessThanQuantity = null;
            }
        }
        return familiesLessThanQuantity;
    }

    public int countFamiliesWithMemberNumber(int quantity) {
        int counter = 0;
        for (Family family : familyDao.getAllFamilies()) {
            if (family.countFamily() == quantity) counter++;
            else if (family.countFamily() != quantity) continue;
            else System.out.println("No such families");
        }
        return counter;
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        Human child = defineGender();
        family.addChild(child);
        child.setFamily(family);
        if (child instanceof Man) {
            child.setName(boyName);
        } else {
            child.setName(girlName);
        }
        child.setSurname(family.getFather().getSurname());
        child.setBirthDate(new Date().getTime());
        child.setIq((family.getFather().getIq() + family.getMother().getIq() / 2));
        return family;
    }

    private Human defineGender() {
        Random r = new Random();
        int probability = r.nextInt();
        if (probability <= 0.5) return new Woman();
        else return new Man();
    }

    public Family adoptChild(Family family, Human child) {
        child.setSurname(family.getFather().getSurname());
        family.addChild(child);
        child.setFamily(family);
        return family;
    }

    public List<Family> deleteAllChildrenOlderThen(int age) throws ParseException {
        for (Family familyItem : familyDao.getAllFamilies()) {
            for (Iterator<Human> iter = familyItem.getChildren().iterator(); iter.hasNext(); ) {
                Human child = iter.next();
                if (child.getHumanAge() >= age) {
                    iter.remove();
                }
            }
        }
        return familyDao.getAllFamilies();
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }


    public Set<Pet> getPets(int familyIndex) {
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            return null;
        } else {
            return familyDao.getAllFamilies().get(familyIndex).getPet();
        }
    }

    public Family addPet(int familyIndex, Pet pet) {
        Family family;
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            family = null;
        } else {
            family = familyDao.getAllFamilies().get(familyIndex);
            if (family.getPet() == null) {
                Set<Pet> pets = new HashSet<>();
                pets.add(pet);
                family.setPet(pets);
            } else {
                family.getPet().add(pet);
            }
        }
        return family;
    }
}
