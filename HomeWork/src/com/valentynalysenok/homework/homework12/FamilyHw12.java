package com.valentynalysenok.homework.homework12;

import com.valentynalysenok.homework.homework12.Animals.Dog;
import com.valentynalysenok.homework.homework12.Animals.DomesticCat;
import com.valentynalysenok.homework.homework12.Animals.Pet;
import com.valentynalysenok.homework.homework12.Human.Human;

import java.util.*;

public class FamilyHw12 {

    public static void main(String[] args) {

        Set<String> petHabits = new HashSet<>();
        petHabits.add("scratching");
        petHabits.add("licking");
        petHabits.add("stealing food");
        petHabits.add("sleeping");

        Set<Pet> pets = new HashSet<>();
        Set<Pet> pets2 = new HashSet<>();
        Set<Pet> pets3 = new HashSet<>();
        Set<Pet> pets4 = new HashSet<>();

        DomesticCat cat = new DomesticCat("Kilka", 11, 89, petHabits);
        DomesticCat cat2 = new DomesticCat("Pushinka", 8, 69, petHabits);
        DomesticCat cat3 = new DomesticCat("Birulka", 5, 92, petHabits);
        Dog dog = new Dog("Bobik", 1, 79, petHabits);
        Dog dog2 = new Dog("Sharik", 4, 39, petHabits);

        pets.add(cat);
        pets2.add(cat2);
        pets2.add(dog2);
        pets3.add(dog);
        pets4.add(cat3);

        Map<String, String> motherSchedule = new HashMap<>();
        motherSchedule.put(DayOfWeek.MONDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.TUESDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.WEDNESDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.THURSDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.FRIDAY.day(), "Go to work");
        motherSchedule.put(DayOfWeek.SATURDAY.day(), "Go to the gym");
        motherSchedule.put(DayOfWeek.SUNDAY.day(), "Cleaning apartment");

        Map<String, String> fatherSchedule = new HashMap<>();
        fatherSchedule.put(DayOfWeek.MONDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.TUESDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.WEDNESDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.THURSDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.FRIDAY.day(), "Go to work");
        fatherSchedule.put(DayOfWeek.SATURDAY.day(), "Reading book");
        fatherSchedule.put(DayOfWeek.SUNDAY.day(), "Go to the gym");

        Human mother = new Human("Ella", "Mask", 1976, 100, motherSchedule);
        Human father = new Human("Nick", "Mask", 1970, 110, fatherSchedule);
        Family family = new Family(mother, father, pets);
        family.bornChild();

        Human mother2 = new Human("Bella", "Trump", 1970, 150, motherSchedule);
        Human father2 = new Human("Igor", "Trump", 1960, 90, fatherSchedule);
        Family family2 = new Family(mother2, father2, pets2);
        family2.bornChild();

        Human mother3 = new Human("Mila", "Rockefeller", 1982, 118, motherSchedule);
        Human father3 = new Human("Kevin", "Rockefeller", 1983, 111, fatherSchedule);
        Family family3 = new Family(mother3, father3, pets3);
        family3.bornChild();

        Human mother4 = new Human("Ursula", "Hilton", 1990, 126, motherSchedule);
        Human father4 = new Human("Piter", "Hilton", 1979, 121, fatherSchedule);
        Family family4 = new Family(mother4, father4, pets4);
        family4.bornChild();

        List<Family> families = new ArrayList<>();
        FamilyDao familyDao = new CollectionFamilyDao(families);
        familyDao.saveFamily(family);
        familyDao.saveFamily(family2);
        familyDao.saveFamily(family3);
        familyDao.saveFamily(family4);
        //System.out.println(collectionFamilies.getFamilyByIndex(1));
        familyDao.deleteFamily(0);
        familyDao.deleteFamily(family2);
        familyDao.deleteFamily(family3);
        System.out.println(familyDao.getAllFamilies());

    }
}
