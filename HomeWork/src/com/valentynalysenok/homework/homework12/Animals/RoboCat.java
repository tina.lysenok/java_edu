package com.valentynalysenok.homework.homework12.Animals;


public class RoboCat extends Pet {

    public RoboCat(String nickname, int age) {
        super(nickname, age);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!");
    }

}
