package com.valentynalysenok.homework.homework12.Human;

import com.valentynalysenok.homework.homework12.Animals.Pet;
import com.valentynalysenok.homework.homework12.Family;

import java.util.*;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;

    private Map<String, String> arrSchedule;

    public Human(String name, String surname, int year, int iq, Map<String, String> arrSchedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
    }

    public Human(String name, String surname, int year, int iq, Map<String, String> arrSchedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
        this.family = family;
    }

    ///конструктор детей
    public Human(int year, Map<String, String> arrSchedule) {
        this.year = year;
        this.arrSchedule = arrSchedule;
    }

    ////пустой конструктор
    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getArrSchedule() {
        return arrSchedule;
    }

    public void setArrSchedule(Map<String, String> arrSchedule) {
        this.arrSchedule = arrSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void describePet() {
        String petTricks;
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            if (pet.getTrickLevel() <= 50 && pet.getTrickLevel() > 0) {
                petTricks = "almost not tricky";
            } else if (pet.getTrickLevel() > 50) {
                petTricks = "very tricky pet";
            } else {
                petTricks = "very clever and very tricky";
            }
            if (pet.getAge() == 0) {
                System.out.println("I have " + pet.getSpecies().getSpeciesName() + ", it's " + petTricks + ".");
            } else {
                System.out.println("I have " + pet.getSpecies().getSpeciesName() + ", it's " + pet.getAge() + " years, it " + petTricks + ".");
            }
        }
    }

    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            if (isTimeToFeedPet == true) {
                System.out.println("Hmm....I'll feed the " + pet.getNickname());
            } else {
                if (pet.getTrickLevel() > randomLevel) {
                    System.out.println("Hmm....I'll feed the " + pet.getNickname());
                } else {
                    System.out.println("I think, " + pet.getNickname() + " don't hungry.");
                }
            }
        }
        return isTimeToFeedPet;
    }

    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "\nHuman{";
        aboutMyself += "name: " + this.name;
        aboutMyself += ", surname: " + this.surname;
        aboutMyself += ", year: " + this.year;
        aboutMyself += ", iq: " + this.iq + "}";
        aboutMyself += "\nMy schedule: ";
        if (this.arrSchedule == null || this.arrSchedule.size() == 0) {
            aboutMyself += "none";
        } else {
            for (Map.Entry<String, String> item : this.arrSchedule.entrySet()) {
                aboutMyself += "\n" + (item.getKey() + ": " + item.getValue());
            }
        }
        return aboutMyself;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(year, human.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }
}
