package com.valentynalysenok.homework.homework5;

public class FamilyHw5 {
    public static void main(String[] args) {

        System.out.println("\nFirst Family:\n");

        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};
        String[][] humanShedule = new String[][]{
                {"Saturday", "Go to the gym. Shopping food. Reading book."},
                {"Sunday", "Cleaning apartment. Take a rest."}
        };
        Pet pet1 = new Pet("cat", "Kilka", 7, 67, petHabits);
        Human mother = new Human("Melissa", "Hilton", 1961, 124);
        Human father = new Human("Bob", "Hilton", 1959, 115);
        Human daughter = new Human("Ella", "Hilton", 1982, 120, mother, father, pet1, humanShedule);

        System.out.println(mother.toString());
        System.out.println(father.toString());
        System.out.println(daughter.toString());
        System.out.println(daughter.describePet());
        daughter.greetPet();
        daughter.feedPet(false);

        System.out.println("\nAbout Pet:\n");

        System.out.println(pet1.toString());
        pet1.eat();
        pet1.foul();
        pet1.respond();

        System.out.println("\nSecond Family:\n");

        Human mother2 = new Human("Lilly", "Trump", 1970);
        Human father2 = new Human("Billy", "Trump", 1971);
        Human daughter2 = new Human("Emma", "Trump", 1994, mother, father);
        System.out.println(mother2.toString());
        System.out.println(father2.toString());
        System.out.println(daughter2.toString());

        System.out.println("\nThird Family:\n");

        Pet pet2 = new Pet("raccon", "Ronny");
        String[][] humanShedule2 = new String[][]{
                {"Saturday", "Go to the gym. Shopping food. Reading book."},
                {"Sunday", "Cleaning apartment. Take a rest."}
        };
        Human mother3 = new Human("Tilda", "Rumus", 1976);
        Human father3 = new Human("Teodor", "Rumus", 1978);
        Human son3 = new Human("Tim", "Rumus", 1997, 123, mother3, father3, pet2, humanShedule2);
        System.out.println(mother3.toString());
        System.out.println(father3.toString());
        System.out.println(son3.toString());
        System.out.println(son3.describePet());
        son3.greetPet();
        son3.feedPet(false);

        System.out.println("\nAbout Pet:\n");

        System.out.println(pet2.toString());
        pet2.eat();
        pet2.foul();
        pet2.respond();

    }
}
