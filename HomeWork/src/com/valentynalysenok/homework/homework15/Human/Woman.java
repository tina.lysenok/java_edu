package com.valentynalysenok.homework.homework15.Human;

import com.valentynalysenok.homework.homework15.Animals.Pet;

import java.util.ArrayList;
import java.util.List;

public final class Woman extends Human {
    
    public void greetPet() {
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            System.out.println("Hi, " + pet.getNickname()
                    + " I'm your mommy, I bring you a delicious food snacks");
        }

    }

    public void makeUp() {
        System.out.println("I'm finished my make up");
    }

}
