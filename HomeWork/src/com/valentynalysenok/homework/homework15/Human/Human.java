package com.valentynalysenok.homework.homework15.Human;

import com.valentynalysenok.homework.homework15.Animals.Pet;
import com.valentynalysenok.homework.homework15.Family;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private String dateOfBirth;
    private int iq;
    private Family family;

    private Map<String, String> arrSchedule;

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<String, String> arrSchedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<String, String> arrSchedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.arrSchedule = arrSchedule;
        this.family = family;
    }

    ///конструктор детей
    public Human(long birthDate, Map<String, String> arrSchedule) {
        this.birthDate = birthDate;
        this.arrSchedule = arrSchedule;
    }

    ///конструктор усыновленных детей детей
    public Human(String name, String surname, String dateOfBirth, int iq) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iq = iq;
        this.setBirthDate(0);
    }

    ////пустой конструктор
    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getDateBirthAdoptChild() {
        return dateOfBirth;
    }

    public void setDateBirthAdoptChild(String dateBirthAdoptChild) {
        this.dateOfBirth = dateBirthAdoptChild;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getArrSchedule() {
        return arrSchedule;
    }

    public void setArrSchedule(Map<String, String> arrSchedule) {
        this.arrSchedule = arrSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void describePet() {
        String petTricks;
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            if (pet.getTrickLevel() <= 50 && pet.getTrickLevel() > 0) {
                petTricks = "almost not tricky";
            } else if (pet.getTrickLevel() > 50) {
                petTricks = "very tricky pet";
            } else {
                petTricks = "very clever and very tricky";
            }
            if (pet.getAge() == 0) {
                System.out.println("I have " + pet.getSpecies().getSpeciesName() + ", it's " + petTricks + ".");
            } else {
                System.out.println("I have " + pet.getSpecies().getSpeciesName() + ", it's " + pet.getAge() + " years, it " + petTricks + ".");
            }
        }
    }

    public boolean feedPet(boolean isTimeToFeedPet) {
        Random random = new Random();
        int randomLevel = random.nextInt(100);
        System.out.println(randomLevel);
        List<Pet> pets = new ArrayList<>(this.getFamily().getPet());
        for (int i = 0; i < pets.size(); i++) {
            Pet pet = pets.get(i);
            if (isTimeToFeedPet == true) {
                System.out.println("Hmm....I'll feed the " + pet.getNickname());
            } else {
                if (pet.getTrickLevel() > randomLevel) {
                    System.out.println("Hmm....I'll feed the " + pet.getNickname());
                } else {
                    System.out.println("I think, " + pet.getNickname() + " don't hungry.");
                }
            }
        }
        return isTimeToFeedPet;
    }

    public int getHumanAge() throws ParseException {
        long diff;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        if (birthDate == 0) {
            long dateBirthOfAdoptChild = format.parse(dateOfBirth).getTime();
            diff = new Date().getTime() - dateBirthOfAdoptChild;
        } else {
            diff = new Date().getTime() - birthDate;
        }
        int years = (int) (diff / (24 * 60 * 60 * 1000)) / 365;
        return years;
    }

    public String describeAge() throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        if (birthDate == 0) {
            calendar.setTimeInMillis(format.parse(dateOfBirth).getTime());
        } else {
            calendar.setTimeInMillis(birthDate);
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        LocalDate dateNow = LocalDate.now();
        LocalDate dateOfBirth = LocalDate.of(year, month, day);
        LocalDate dateBirthOfYearNow = LocalDate.of(new Date().getYear(), month, day);
        int fullYears =  Period.between(dateOfBirth, dateNow).getYears();
        int fullMonth = Period.between(dateBirthOfYearNow, dateNow).getMonths();
        int fullDays = Period.between(dateBirthOfYearNow, dateNow).getDays();
        return fullYears + " years " + fullMonth + " month and " + fullDays + " days";
    }

    public String convertDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(birthDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (month < 10 && day < 10) {
            return "0" + day + "/" + "0" + month + "/" + year;
        } else if (month < 10) {
            return day + "/" + "0" + month + "/" + year;
        } else if (day < 10) {
            return "0" + day + "/" + month + "/" + year;
        } else {
            return day + "/" + month + "/" + year;
        }
    }

    @Override
    public String toString() {
        String aboutMyself = "";
        aboutMyself += "\nHuman{";
        aboutMyself += "name: " + name;
        aboutMyself += ", surname: " + surname;
        if (birthDate == 0) {
            aboutMyself += ", date of birth: " + dateOfBirth;
        } else {
            aboutMyself += ", date of birth: " + convertDate();
        }
        aboutMyself += ", iq: " + iq + "}";
        aboutMyself += "\nMy schedule: ";
        if (arrSchedule == null || arrSchedule.size() == 0) {
            aboutMyself += "none";
        } else {
            for (Map.Entry<String, String> item : arrSchedule.entrySet()) {
                aboutMyself += "\n" + (item.getKey() + ": " + item.getValue());
            }
        }
        return aboutMyself;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(birthDate, human.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }

}
