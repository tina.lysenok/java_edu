package com.valentynalysenok.homework.homework2;

public class Table {
    public static void main(String[] args) {
        int[] arr = new int[11];
        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j < arr.length; j++) {
                System.out.printf("%4d", i*j);
            }
            System.out.println();
        }
        System.out.println();
        int[] pif = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int q : pif) {
            for (int w : pif) {
                System.out.printf("%4d", (q * w));
            }
            System.out.println();
        }
        System.out.println("It's Windows path: \"C:\\ProgramFiles\\Java\\jdk1.7.0\\bin\\" + "\nIt's Java string: \\\"C:\\Program Files\\Java\\jdk1.7.0\\bin\"");
        System.out.println(sumDigitsInNumber(546));
    }
    public static int sumDigitsInNumber(int number) {
        int sum = 0;
        while(number > 0) {
            int n = number % 10;
            sum += n;
            number = number / 10;
        }
        return sum;
    }

}
