package com.valentynalysenok.homework.homework10;

import com.valentynalysenok.homework.homework10.Animals.DomesticCat;
import com.valentynalysenok.homework.homework10.Human.Human;
import com.valentynalysenok.homework.homework10.Human.Man;
import com.valentynalysenok.homework.homework10.Human.Woman;

public class FamilyHw10 {

    public static void main(String[] args) {

        String[] petHabits = new String[]{"jumping", "scratching", "licking", "stealing food", "sleeping"};

        DomesticCat cat = new DomesticCat("Kilka", 11, 89, petHabits);

        String[][] motherSchedule = new String[][]{
                {DayOfWeek.MONDAY.day(), "Go to work"},
                {DayOfWeek.TUESDAY.day(), "Go to work"},
                {DayOfWeek.WEDNESDAY.day(), "Go to work"},
                {DayOfWeek.THURSDAY.day(), "Go to work"},
                {DayOfWeek.FRIDAY.day(), "Go to work"},
                {DayOfWeek.SATURDAY.day(), "Go to the gym"},
                {DayOfWeek.SUNDAY.day(), "Cleaning apartment"}
        };
        String[][] fatherSchedule = new String[][]{
                {DayOfWeek.MONDAY.day(), "Go to work"},
                {DayOfWeek.TUESDAY.day(), "Go to work"},
                {DayOfWeek.WEDNESDAY.day(), "Go to work"},
                {DayOfWeek.THURSDAY.day(), "Go to work"},
                {DayOfWeek.FRIDAY.day(), "Go to work"},
                {DayOfWeek.SATURDAY.day(), "Reading book"},
                {DayOfWeek.SUNDAY.day(), "Go to the gym"}
        };

        Human mother = new Human("Ella", "Tuck", 1980, 110, motherSchedule);
        Human father = new Human("Nick", "Tuck", 1980, 120, fatherSchedule);

        Family family = new Family(mother, father, cat);

        Human child = family.bornChild();

        System.out.println(family.countFamily());
        System.out.println(family.toString());

        System.out.println(child instanceof Man);
        System.out.println(child instanceof Woman);

    }
}
