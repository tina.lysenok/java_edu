package com.valentynalysenok.homework.homework10;

import com.valentynalysenok.homework.homework10.Animals.Pet;
import com.valentynalysenok.homework.homework10.Human.Human;
import com.valentynalysenok.homework.homework10.Human.Man;
import com.valentynalysenok.homework.homework10.Human.Woman;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    //единственным условием создания новой семьи является наличие 2-х родителей, при этом у родителей
    //должна устанавливаться ссылка на текущую новую семью, а семья создается с пустым массивом детей.
    //для того, чтобы получить у человека инфу о его семье - мы сможем это сделать по ссылке mother.setFamily(this);
    public Family(Human mother, Human father, Pet pet) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
        }
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new Human[0];
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        this.children = Arrays.copyOf(this.children, this.children.length + 1);
        this.children[this.children.length - 1] = child;
        //для того, чтобы получить у ребенка инфу о его семье - мы сможем это сделать по ссылке child.setFamily(this);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        int i;
        for (i = 0; i <= this.children.length; i++) {
            if (i == index) {
                break;
            }
        }
        for (int j = i; j < this.children.length - 1; j++) {
            this.children[j] = this.children[j + 1];
        }
        this.children = Arrays.copyOf(this.children, this.children.length - 1);
        return true;
    }

    public boolean deleteChild(Human human) {
        int i;
        for (i = 0; i <= this.children.length; i++) {
            if (children[i].equals(human)) {
                break;
            }
        }
        for (int j = i; j < this.children.length - 1; j++) {
            this.children[j] = this.children[j + 1];
        }
        this.children = Arrays.copyOf(this.children, this.children.length - 1);
        return true;
    }

    public int countFamily() {
        int count = 0;
        count += this.children.length;
        if (this.mother != null && this.father != null) {
            count += 2;
        } else if (this.mother == null || this.father == null) {
            count += 1;
        } else {
            count += 0;
        }
        return count;
    }

    @Override
    public Human bornChild() {
        Human child = defineGender();
        child.setFamily(this);
        addChild(child);
        child.setName(chooseName(child));
        child.setSurname(father.getSurname());
        child.setIq(defineIq());
        child.setYear(2010);
        return child;
    }

    private Human defineGender() {
        Random r = new Random();
        int probability = r.nextInt();
        if (probability <= 0.5) {
            return new Woman();
        } else {
            return new Man();
        }
    }

    private String chooseName(Human child) {
        String name = new String();
        Random random = new Random();
        int r = random.nextInt(namesBoys.length);
        if (child instanceof Man) {
            for (int i = 0; i < namesBoys.length; i++) {
                name = namesBoys[r];
            }
        } else {
            for (int i = 0; i < namesGirls.length; i++) {
                name = namesGirls[r];
            }
        }
        return name;
    }

    private int defineIq() {
        return (this.mother.getIq() + this.father.getIq()) / 2;
    }

    @Override
    public String toString() {
        String aboutFamily = "";
        aboutFamily += "Family {";

        if (this.mother != null) {
            aboutFamily += "\nMother: " + this.mother;
        } else {
            aboutFamily += "Mother: " + null;
        }
        if (this.father != null) {
            aboutFamily += ",\nFather: " + this.father;
        } else {
            aboutFamily += ", Father: " + null + ", ";
        }

        aboutFamily += ",\nChildren: " + Arrays.toString(this.children);

        if (this.pet == null) {
        } else {
            aboutFamily += ";\nPet: " + this.pet + "}";
        }
        return aboutFamily;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return  Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("CLEAN: " + this.hashCode());
    }
}
