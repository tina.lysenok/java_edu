package com.valentynalysenok.homework.homework10.Human;

public final class Woman extends Human {
    
    public void greetPet() {
        System.out.println("Hi, " + this.getFamily().getPet().getNickname()
                + " I'm your mommy, I bring you a delicious food snacks");
    }

    public void makeUp() {
        System.out.println("I'm finished my make up");
    }

}
