package com.valentynalysenok.homework.homework10.Human;

public final class Man extends Human {

    public void greetPet() {
        System.out.println("Hi, " + this.getFamily().getPet().getNickname() + " I'm your daddy, so bring me my beer!");
    }

    public void repairCar() {
        System.out.println("I'm enjoy of repairing my car");
    }

}
