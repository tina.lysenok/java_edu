package com.valentynalysenok.homework.homework16;

import com.valentynalysenok.homework.homework16.Animals.Pet;
import com.valentynalysenok.homework.homework16.Human.Human;
import com.valentynalysenok.homework.homework16.Human.Man;
import com.valentynalysenok.homework.homework16.Human.Woman;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public void setFamilyDao(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void displayAllFamilies() {
        if (familyDao.getAllFamilies() == null || familyDao.getAllFamilies().size() == 0) {
            System.out.println("The list of families is empty.");
        } else {
            familyDao.getAllFamilies().forEach(family -> family.prettyFormat());
        }
    }

    public List<Family> getFamiliesBiggerThan(int quantity) {
        List<Family> familiesBiggerThanQuantity = new ArrayList<>();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() > quantity) {
                familiesBiggerThanQuantity.add(family);
                family.prettyFormat();
            }
        });
        if (familiesBiggerThanQuantity.size() == 0) {
            System.out.println("No such families");
        }
        return familiesBiggerThanQuantity;
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        List<Family> familiesLessThanQuantity = new ArrayList<>();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() < quantity) {
                familiesLessThanQuantity.add(family);
                family.prettyFormat();
            }
        });
        if (familiesLessThanQuantity.size() == 0) {
            System.out.println("No such families");
        }
        return familiesLessThanQuantity;
    }

    public AtomicInteger countFamiliesWithMemberNumber(int quantity) {
        AtomicInteger counter = new AtomicInteger();
        familyDao.getAllFamilies().forEach(family -> {
            if (family.countFamily() == quantity) {
                counter.getAndIncrement();
            }
        });
        return counter;
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
        return family;
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        Human child = defineGender();
        family.addChild(child);
        child.setFamily(family);
        if (child instanceof Man) {
            child.setName(boyName);
        } else {
            child.setName(girlName);
        }
        child.setSurname(family.getFather().getSurname());
        child.setBirthDate(new Date().getTime());
        child.setIq(((family.getFather().getIq() + family.getMother().getIq()) / 2));
        return family;
    }

    public Family bornChild(int familyId, String boyName, String girlName) {
        List<Family> families = familyDao.getAllFamilies();
        Family family;
        if (families != null && familyId >= 0 && familyId < families.size()) {
            family = familyDao.getFamilyByIndex(familyId);
        } else {
            System.out.println("Your FamilyDB is empty or family index is wrong.");
            return null;
        }
        Human child = defineGender();
        family.addChild(child);
        child.setFamily(family);
        if (child instanceof Man) child.setName(boyName);
        else child.setName(girlName);
        child.setSurname(family.getFather().getSurname());
        child.setBirthDate(new Date().getTime());
        child.setIq(((family.getFather().getIq() + family.getMother().getIq()) / 2));

        return family;
    }

    private Human defineGender() {
        Random r = new Random();
        int probability = r.nextInt();
        if (probability <= 0.5) return new Woman();
        else return new Man();
    }

    public Family adoptChild(Family family, Human child) {
        child.setSurname(family.getFather().getSurname());
        family.addChild(child);
        child.setFamily(family);
        return family;
    }

    public Family adoptChild(int familyId, Human child) {
        List<Family> families = familyDao.getAllFamilies();
        Family family;
        if (families != null && familyId >= 0 && familyId < families.size()) {
            family = familyDao.getFamilyByIndex(familyId);
        } else {
            System.out.println("Your FamilyDB is empty or family index is wrong.");
            return null;
        }
        child.setSurname(family.getFather().getSurname());
        family.addChild(child);
        child.setFamily(family);
        return family;
    }

    public List<Family> deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(family -> {
            for (Iterator<Human> iter = family.getChildren().iterator(); iter.hasNext(); ) {
                Human child = iter.next();
                try {
                    if (child.getHumanAge() >= age) {
                        iter.remove();
                        System.out.println("The child was deleted from next family/ies:");
                        family.prettyFormat();
                    } else {
                        System.out.println("No children in FamilyDB which elder " + age + " years.");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        return familyDao.getAllFamilies();
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }


    public Set<Pet> getPets(int familyIndex) {
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            return null;
        } else {
            return familyDao.getAllFamilies().get(familyIndex).getPets();
        }
    }

    public Family addPet(int familyIndex, Pet pet) {
        Family family;
        if (familyIndex < 0 || familyIndex >= familyDao.getAllFamilies().size()) {
            family = null;
        } else {
            family = familyDao.getAllFamilies().get(familyIndex);
            if (family.getPets() == null) {
                Set<Pet> pets = new HashSet<>();
                pets.add(pet);
                family.setPets(pets);
            } else {
                family.getPets().add(pet);
            }
        }
        return family;
    }
}
