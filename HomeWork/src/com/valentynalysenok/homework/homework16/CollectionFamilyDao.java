package com.valentynalysenok.homework.homework16;

import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families;

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) return null;
        else return families.get(index);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        if (index < 0 || index >= families.size()) return false;
        else families.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public Family saveFamily(Family family) {
        if (families.indexOf(family) == -1) {
            families.add(family);
        } else {
            families.set(families.indexOf(family), family);
        }
        return family;
    }

}
